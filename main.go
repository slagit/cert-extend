package main

import (
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"os"
	"time"
)

var (
	ErrNoCertificate = errors.New("No Certificate found")
)

func loadCertificate(filename string) (*x509.Certificate, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	data, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}
	for len(data) > 0 {
		var blk *pem.Block
		blk, data = pem.Decode(data)
		if blk == nil {
			break
		}
		if blk.Type == "CERTIFICATE" {
			return x509.ParseCertificate(blk.Bytes)
		}
	}
	return nil, ErrNoCertificate
}

func loadPrivateKey(filename string) (*ecdsa.PrivateKey, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	data, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}
	for len(data) > 0 {
		var blk *pem.Block
		blk, data = pem.Decode(data)
		if blk == nil {
			break
		}
		if blk.Type == "EC PRIVATE KEY" {
			return x509.ParseECPrivateKey(blk.Bytes)
		}
		// TODO: Check for other key types and return if found
	}
	return nil, ErrNoCertificate
}

func main() {
	caCert, err := loadCertificate("ca.pem")
	if err != nil {
		panic(fmt.Errorf("Error loading ca certificate: %s", err))
	}

	caKey, err := loadPrivateKey("ca-key.pem")
	if err != nil {
		panic(fmt.Errorf("Error loading ca certificate: %s", err))
	}

	orig, err := loadCertificate("original.pem")
	if err != nil {
		panic(fmt.Errorf("Error loading original certificate: %s", err))
	}

	orig.NotAfter = time.Date(
		orig.NotAfter.Year() + 1,
		orig.NotAfter.Month(),
		orig.NotAfter.Day(),
		orig.NotAfter.Hour(),
		orig.NotAfter.Minute(),
		orig.NotAfter.Second(),
		orig.NotAfter.Nanosecond(),
		orig.NotAfter.Location(),
	)

	blk := pem.Block{
		Type: "CERTIFICATE",
	}
	blk.Bytes, err = x509.CreateCertificate(rand.Reader, orig, caCert, orig.PublicKey, caKey)
	if err != nil {
		panic(err)
	}

	update, err := os.Create("updated.pem")
	if err != nil {
		panic(err)
	}
	if err := pem.Encode(update, &blk); err != nil {
		update.Close()
		panic(err)
	}
	if err := update.Close(); err != nil {
		panic(err)
	}
}

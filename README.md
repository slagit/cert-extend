# Cert Extend

This tool adds one year to an existing certificate and re-signs the
certificate without changing any other values.

This is a quick hack for extending kubernetes certificates to get things
working *now* when certificates expire. Actually using this is a bad idea,
and real certificate rotation should be fixed ASAP.

## Usage

Place the following inputs:

* `ca.pem` - CA Certificate
* `ca-key.pem` - CA Private Key (Currently ECDSA only)
* `original.pem` - Original, potentially expired, certificate

Run the tool:

```sh
go run .
```

A certificate with the expiration extended by one year will be placed in
`updated.pem`.
